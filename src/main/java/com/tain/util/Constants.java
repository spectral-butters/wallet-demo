package com.tain.util;

public interface Constants {
    String EMAIL_REGEXP = "^[\\w-_.+]*[\\w-_.]@([\\w-_]+\\.)+[\\w]+[\\w]$";
    String PASSWORD_REGEXP = "^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d).{8,}$";
}
