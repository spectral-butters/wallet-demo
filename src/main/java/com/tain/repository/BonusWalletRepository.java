package com.tain.repository;

import com.tain.entity.dao.BonusWallet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BonusWalletRepository extends JpaRepository<BonusWallet, Long> {

}
