package com.tain.repository;

import com.tain.entity.dao.GameRound;
import com.tain.util.RoundEventType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GameRoundRepository extends JpaRepository<GameRound, Long> {

    List<GameRound> findByUserId(Long userId);

    Optional<GameRound> findByTransactionIdAndRoundType(String transactionId, RoundEventType roundType);

}
