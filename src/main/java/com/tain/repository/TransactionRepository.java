package com.tain.repository;

import com.tain.entity.dao.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Page<Transaction> findAll(Specification<Transaction> specification, Pageable pageable);

    List<Transaction> findAllByUserId(Long userId);

}
