package com.tain.repository;

import com.tain.entity.dao.CashWalletTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface CashWalletTransactionRepository extends JpaRepository<CashWalletTransaction, Long> {

    @Query("SELECT COALESCE(SUM(t.amount), 0) FROM CashWalletTransaction t WHERE t.createdAt > :createdAt AND t.cashWallet.id = :id")
    BigDecimal getBalance(@Param("createdAt") LocalDateTime createdAt, @Param("id") Long id);

}
