package com.tain.repository.specification;

import com.tain.entity.dao.Transaction;
import com.tain.entity.dto.request.TransactionSearchRequest;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Data
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class TransactionSpecification implements Specification<Transaction> {

    TransactionSearchRequest request;

    @Override
    public Predicate toPredicate(Root<Transaction> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (request.getId() != null) {
            predicates.add(criteriaBuilder.equal(root.get("id"), request.getId()));
        }

        if (request.getUserId() != null) {
            predicates.add(criteriaBuilder.equal(root.get("user").get("id"), request.getUserId()));
        }

        if (request.getTransactionType() != null) {
            predicates.add(criteriaBuilder.equal(root.get("transactionType"), request.getTransactionType()));
        }

        if (request.getTransactionId() != null) {
            predicates.add(criteriaBuilder.equal(root.get("transactionId"), request.getTransactionId()));
        }

        if (request.getWalletType() != null) {
            predicates.add(criteriaBuilder.equal(root.get("walletType"), request.getWalletType()));
        }

        if (request.getNote() != null) {
            predicates.add(criteriaBuilder.like(root.get("note"), "%" + request.getNote() + "%"));
        }

        if (request.getAmountFrom() != null) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("amount"), request.getAmountFrom()));
        }

        if (request.getAmountTo() != null) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("amount"), request.getAmountTo()));
        }

        if (request.getFrom() != null) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), request.getFrom()));
        }

        if (request.getTo() != null) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("createdAt"), request.getTo()));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

}
