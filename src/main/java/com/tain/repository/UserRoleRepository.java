package com.tain.repository;

import com.tain.entity.dao.UserRole;
import com.tain.util.UserRoles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

    Optional<UserRole> findByRole(UserRoles role);

}
