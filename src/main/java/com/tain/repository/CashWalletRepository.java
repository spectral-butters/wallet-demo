package com.tain.repository;

import com.tain.entity.dao.CashWallet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CashWalletRepository extends JpaRepository<CashWallet, Long> {
}
