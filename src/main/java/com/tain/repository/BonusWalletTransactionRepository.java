package com.tain.repository;

import com.tain.entity.dao.BonusWalletTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface BonusWalletTransactionRepository extends JpaRepository<BonusWalletTransaction, Long> {
    @Query("SELECT COALESCE(SUM(t.amount), 0) FROM BonusWalletTransaction t WHERE t.createdAt > :createdAt AND t.bonusWallet.id = :id")
    BigDecimal getBalance(@Param("createdAt") LocalDateTime lastBalanceUpdate, @Param("id") Long id);
}
