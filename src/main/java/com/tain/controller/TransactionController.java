package com.tain.controller;

import com.tain.entity.dao.Transaction;
import com.tain.entity.dao.User;
import com.tain.entity.dto.TransactionResponseDto;
import com.tain.entity.dto.request.TransactionRequest;
import com.tain.entity.dto.request.TransactionSearchRequest;
import com.tain.entity.dto.response.UserBalanceResponse;
import com.tain.entity.mapper.TransactionMapper;
import com.tain.service.TransactionService;
import com.tain.service.UserService;
import com.tain.service.security.CustomUserDetails;
import com.tain.util.TransactionType;
import com.tain.util.WalletType;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class TransactionController {

    TransactionService transactionService;
    UserService userService;

    @PostMapping("/bet")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('USER')")
    public UserBalanceResponse processBet(@Valid @RequestBody TransactionRequest request,
                                          @ApiIgnore @AuthenticationPrincipal CustomUserDetails userDetails) {
        User user = userService.getById(userDetails.getId());
        request.setPlayerId(user.getExternalId());
        log.info("Processing bet: {}", request);
        return transactionService.processBet(request);
    }

    @PostMapping("/withdraw")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('USER')")
    public UserBalanceResponse processWithdraw(@Valid @RequestBody TransactionRequest request,
                                               @ApiIgnore @AuthenticationPrincipal CustomUserDetails userDetails) {
        User user = userService.getById(userDetails.getId());
        request.setPlayerId(user.getExternalId());
        log.info("Processing withdrawal: {}", request);
        return transactionService.processWithdrawal(request);
    }

    @PostMapping("/deposit")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('USER')")
    public UserBalanceResponse processDeposit(@Valid @RequestBody TransactionRequest request,
                                              @ApiIgnore @AuthenticationPrincipal CustomUserDetails userDetails) {
        User user = userService.getById(userDetails.getId());
        request.setPlayerId(user.getExternalId());
        log.info("Processing deposit: {}", request);
        return transactionService.processDeposit(request);
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('USER')")
    public Page<TransactionResponseDto> searchTransactions(
            @RequestParam(value = "transaction_type", required = false) TransactionType transactionType,
            @RequestParam(value = "transaction_id", required = false) String transactionId,
            @RequestParam(value = "wallet_type", required = false) WalletType walletType,
            @RequestParam(value = "note", required = false) String note,
            @RequestParam(value = "amount_from", required = false) BigDecimal amountFrom,
            @RequestParam(value = "amount_to", required = false) BigDecimal amountTo,
            @RequestParam(value = "created_from", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime from,
            @RequestParam(value = "created_to", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime to,
            @PageableDefault(sort = "id", size = 20) Pageable pageable,
            @ApiIgnore @AuthenticationPrincipal CustomUserDetails userDetails) {
        TransactionSearchRequest searchRequest = TransactionSearchRequest.builder()
                .userId(userDetails.getId())
                .transactionType(transactionType)
                .transactionId(transactionId)
                .walletType(walletType)
                .note(note)
                .amountFrom(amountFrom)
                .amountTo(amountTo)
                .from(from)
                .to(to)
                .build();

        return transactionService.search(searchRequest, pageable)
                .map(TransactionMapper.INSTANCE::toTransactionResponseDto);
    }

}
