package com.tain.controller;

import com.tain.entity.dao.User;
import com.tain.entity.dto.request.CreateUserRequest;
import com.tain.entity.dto.response.LoginResponse;
import com.tain.entity.dto.response.UserBalanceResponse;
import com.tain.service.UserService;
import com.tain.service.security.AuthService;
import com.tain.service.security.CustomUserDetails;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UserController {

    UserService userService;
    AuthService authService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public LoginResponse create(@Valid @RequestBody CreateUserRequest request) {
        userService.create(request);
        return authService.authenticate(request.getEmail(), request.getPassword());
    }

    @GetMapping("/balance")
    @PreAuthorize("hasAnyAuthority('USER')")
    public UserBalanceResponse getBalance(@ApiIgnore @AuthenticationPrincipal CustomUserDetails userDetails) {
        User user = userService.getById(userDetails.getId());
        return userService.getBalance(user);
    }

}
