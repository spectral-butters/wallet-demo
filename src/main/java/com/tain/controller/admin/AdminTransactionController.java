package com.tain.controller.admin;

import com.tain.entity.dao.Transaction;
import com.tain.entity.dto.TransactionResponseDto;
import com.tain.entity.dto.request.TransactionRequest;
import com.tain.entity.dto.request.TransactionSearchRequest;
import com.tain.entity.dto.response.UserBalanceResponse;
import com.tain.entity.mapper.TransactionMapper;
import com.tain.service.TransactionService;
import com.tain.util.TransactionType;
import com.tain.util.WalletType;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/admin/transaction")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AdminTransactionController {

    TransactionService transactionService;

    @PostMapping("/win")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public UserBalanceResponse processWin(@Valid @RequestBody TransactionRequest request) {
        log.info("Processing win: {}", request);
        return transactionService.processWin(request);
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public Page<TransactionResponseDto> searchTransactions(
            @RequestParam(value = "user_id", required = false) Long userId,
            @RequestParam(value = "transaction_type", required = false) TransactionType transactionType,
            @RequestParam(value = "transaction_id", required = false) String transactionId,
            @RequestParam(value = "wallet_type", required = false) WalletType walletType,
            @RequestParam(value = "note", required = false) String note,
            @RequestParam(value = "amount_from", required = false) BigDecimal amountFrom,
            @RequestParam(value = "amount_to", required = false) BigDecimal amountTo,
            @RequestParam(value = "created_from", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime from,
            @RequestParam(value = "created_to", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime to,
            @PageableDefault(sort = "id", size = 20) Pageable pageable) {
        TransactionSearchRequest searchRequest = TransactionSearchRequest.builder()
                .userId(userId)
                .transactionType(transactionType)
                .transactionId(transactionId)
                .walletType(walletType)
                .note(note)
                .amountFrom(amountFrom)
                .amountTo(amountTo)
                .from(from)
                .to(to)
                .build();

        Page<Transaction> transactionPage =  transactionService.search(searchRequest, pageable);

        return transactionPage.map(TransactionMapper.INSTANCE::toTransactionResponseDto);
    }

}
