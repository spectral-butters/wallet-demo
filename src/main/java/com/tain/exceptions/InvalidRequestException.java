package com.tain.exceptions;

import com.tain.util.ErrorCode;
import org.springframework.http.HttpStatus;

public class InvalidRequestException extends ErrorCodeException {

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    public InvalidRequestException(ErrorCode errorCode) {
        super(errorCode);
    }

    public InvalidRequestException(String message, ErrorCode errorCode) {
        super(message, errorCode);
    }

    public InvalidRequestException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }
}
