package com.tain.exceptions;

import com.tain.util.ErrorCode;
import org.springframework.http.HttpStatus;

public class AuthorizationException extends ErrorCodeException {

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.FORBIDDEN;
    }

    public AuthorizationException(ErrorCode errorCode) {
        super(errorCode);
    }

    public AuthorizationException(String message, ErrorCode errorCode) {
        super(message, errorCode);
    }

    public AuthorizationException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }
}
