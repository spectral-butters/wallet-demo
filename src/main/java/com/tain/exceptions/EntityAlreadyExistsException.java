package com.tain.exceptions;

import com.tain.util.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityAlreadyExistsException extends ErrorCodeException {

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.CONFLICT;
    }

    public EntityAlreadyExistsException(ErrorCode errorCode) {
        super(errorCode);
    }

    public EntityAlreadyExistsException(String message, ErrorCode errorCode) {
        super(message, errorCode);
    }

    public EntityAlreadyExistsException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }
}
