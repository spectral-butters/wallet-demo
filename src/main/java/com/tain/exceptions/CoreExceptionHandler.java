package com.tain.exceptions;

import com.tain.entity.dto.response.ErrorResponse;
import com.tain.util.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class CoreExceptionHandler extends ResponseEntityExceptionHandler {

    //called when @RequestParam is missing
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Message: {}; url: {}", e.getMessage(), (((ServletWebRequest) request).getRequest().getRequestURI()), e);
        String errorMsg = e.getMessage();
        return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);
    }

    //show error message when required header is missing
    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Message: {}; url: {}", e.getMessage(), (((ServletWebRequest) request).getRequest().getRequestURI()), e);
        String errorMsg = e.getMessage();
        return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);
    }

    // called when @Valid is violated, to avoid checking BindingResult
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Message: {}; url: {}", ex.getMessage(), (((ServletWebRequest) request).getRequest().getRequestURI()),
                ex);
        String errorMsg = ex.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining("\n"));
        return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ErrorCodeException.class)
    public ResponseEntity<ErrorResponse> handleErrorCodeException(ErrorCodeException e) {
        log.warn("ErrorCode exception={} code={}", e.getMessage(), e.getErrorCode(), e);
        return new ResponseEntity<>(new ErrorResponse(e.getErrorCode(), e.getMessage()), e.getStatus());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorResponse> handleErrorCodeException(AccessDeniedException e) {
        log.warn("AccessDeniedException exception={} code={}", e.getMessage(), ErrorCode.ACCESS_DENIED, e);
        return new ResponseEntity<>(new ErrorResponse(ErrorCode.ACCESS_DENIED, e.getMessage()), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponse> handleRuntimeException(RuntimeException e) {
        log.warn("ErrorCode exception={} code={}", e.getMessage(), e.getMessage(), e);
        return new ResponseEntity<>(new ErrorResponse(ErrorCode.INTERNAL_ERROR, "Internal error. Please contact support"),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
