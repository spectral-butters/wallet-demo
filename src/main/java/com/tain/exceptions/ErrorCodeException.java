package com.tain.exceptions;

import com.tain.util.ErrorCode;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class ErrorCodeException extends RuntimeException {

    ErrorCode errorCode;
    HttpStatus status;

    public HttpStatus getStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public ErrorCodeException(ErrorCode errorCode) {
        super();
        this.errorCode = errorCode;
    }
    public ErrorCodeException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public ErrorCodeException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }
}
