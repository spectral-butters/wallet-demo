package com.tain.entity.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
@JsonNaming(com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserBalanceResponse {

    BigDecimal cashBalance;
    BigDecimal bonusBalance;
    BigDecimal totalBalance;

}
