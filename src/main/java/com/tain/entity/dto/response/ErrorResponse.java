package com.tain.entity.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.tain.util.ErrorCode;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonNaming(value = com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ErrorResponse {

    ErrorCode errorCode;
    String message;

}
