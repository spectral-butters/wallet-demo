package com.tain.entity.dto;

import com.tain.entity.dto.request.TransactionRequest;
import com.tain.util.TransactionType;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class WalletTransactionDto extends TransactionRequest {

    TransactionType transactionType;
    String note;

}
