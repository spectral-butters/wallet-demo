package com.tain.entity.dto;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.tain.entity.dao.User;
import com.tain.util.TransactionType;
import com.tain.util.WalletType;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
@JsonNaming(value = com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TransactionResponseDto {

    Long userId;

    BigDecimal amount;
    String transactionId;
    TransactionType transactionType;
    WalletType walletType;
    String note;
    LocalDateTime createdAt;

}
