package com.tain.entity.dto.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.tain.util.TransactionType;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TransactionRequest {

    @NotEmpty(message = "player_id must be specified")
    String playerId;

    @NotEmpty(message = "transaction_id must be specified")
    String transactionId;

    @NotNull(message = "amount must be specified")
    @Min(value = 0, message = "amount must be greater than 0")
    BigDecimal amount;

}
