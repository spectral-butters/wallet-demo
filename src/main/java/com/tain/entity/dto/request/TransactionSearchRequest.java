package com.tain.entity.dto.request;

import com.tain.util.TransactionType;
import com.tain.util.WalletType;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class TransactionSearchRequest {

    Long id;
    Long userId;

    TransactionType transactionType;
    String transactionId;
    WalletType walletType;

    String note;

    BigDecimal amountFrom;
    BigDecimal amountTo;

    LocalDateTime from;
    LocalDateTime to;

}
