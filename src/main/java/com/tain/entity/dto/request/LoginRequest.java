package com.tain.entity.dto.request;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
@JsonNaming(com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy.class)
public class LoginRequest {

    @NotEmpty(message = "email is required")
    String email;

    @NotEmpty(message = "password is required")
    String password;

}
