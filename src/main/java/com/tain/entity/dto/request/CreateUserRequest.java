package com.tain.entity.dto.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.tain.util.Constants;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CreateUserRequest {

    @Pattern(regexp = Constants.EMAIL_REGEXP, message = "email is not valid")
    @NotEmpty(message = "email must be specified")
    String email;

    @NotEmpty(message = "password must be specified")
    @Pattern(regexp = Constants.PASSWORD_REGEXP, message = "Password must contain a minimum of 8 characters with at least 1 uppercase letter, 1 lowercase letter, and 1 digit")
    String password;

    @NotEmpty(message = "confirm_password must be specified")
    String confirmPassword;

}
