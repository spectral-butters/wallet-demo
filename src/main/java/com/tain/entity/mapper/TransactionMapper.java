package com.tain.entity.mapper;

import com.tain.entity.dao.BonusWalletTransaction;
import com.tain.entity.dao.CashWalletTransaction;
import com.tain.entity.dao.Transaction;
import com.tain.entity.dto.TransactionResponseDto;
import com.tain.entity.dto.WalletTransactionDto;
import com.tain.entity.dto.request.TransactionRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(nullValuePropertyMappingStrategy = org.mapstruct.NullValuePropertyMappingStrategy.IGNORE)
public interface TransactionMapper {

    TransactionMapper INSTANCE = Mappers.getMapper(TransactionMapper.class);
    WalletTransactionDto toWalletTransactionDto(TransactionRequest transactionRequest);
    CashWalletTransaction toCashWalletTransaction(WalletTransactionDto walletTransactionDto);
    BonusWalletTransaction toBonusWalletTransaction(WalletTransactionDto walletTransactionDto);
    Transaction toTransaction(WalletTransactionDto transactionDto);

    @Mappings({
            @Mapping(target = "userId", source = "user.id")
    })
    TransactionResponseDto toTransactionResponseDto(Transaction transaction);
}
