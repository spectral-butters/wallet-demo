package com.tain.entity.dao;

import com.tain.util.UserRoles;
import lombok.AccessLevel;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Data
@Entity(name = "user_roles")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRole implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    Integer id;

    @Column(nullable = false, unique = true)
    @Enumerated(EnumType.STRING)
    UserRoles role;

    @Override
    public String getAuthority() {
        return role.name();
    }
}
