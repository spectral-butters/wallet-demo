package com.tain.entity.dao;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity(name = "users")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false, unique = true)
    String email;

    @Column(nullable = false)
    String password;

    @ManyToMany
    @JoinTable(name = "users_user_roles", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "user_role_id"))
    List<UserRole> roles = new ArrayList<>();

    @Column(nullable = false, unique = true)
    String externalId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cash_wallet_id")
    CashWallet cashWallet;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bonus_wallet_id")
    BonusWallet bonusWallet;

    @Column(nullable = false)
    LocalDateTime createdAt = LocalDateTime.now();

    @Column(nullable = false)
    LocalDateTime updatedAt = LocalDateTime.now();
}
