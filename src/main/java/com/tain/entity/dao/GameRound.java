package com.tain.entity.dao;

import com.tain.util.RoundEventType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"roundType", "transactionId"}),
        @UniqueConstraint(columnNames = {"user_id", "transactionId", "roundType"})})
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameRound {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    User user;

    @Column(nullable = false)
    BigDecimal amount;

    @Column(nullable = false)
    BigDecimal proportion;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    RoundEventType roundType;

    @Column(nullable = false)
    String transactionId;

    @Column(nullable = false)
    LocalDateTime createdAt = LocalDateTime.now();

}
