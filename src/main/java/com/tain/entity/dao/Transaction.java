package com.tain.entity.dao;

import com.tain.util.TransactionType;
import com.tain.util.WalletType;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class Transaction {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    User user;

    @Column(nullable = false)
    BigDecimal amount;

    @Column(nullable = false)
    String transactionId;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    TransactionType transactionType;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    WalletType walletType;

    String note;

    @Column(nullable = false)
    LocalDateTime createdAt = LocalDateTime.now();
}
