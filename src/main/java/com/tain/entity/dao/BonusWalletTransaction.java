package com.tain.entity.dao;

import com.tain.util.TransactionType;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class BonusWalletTransaction {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "bonus_wallet_id", nullable = false)
    BonusWallet bonusWallet;

    @Column(nullable = false, unique = true)
    String transactionId;

    @Column(nullable = false)
    BigDecimal amount;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    TransactionType transactionType;

    String note;

    @Column(nullable = false)
    LocalDateTime createdAt = LocalDateTime.now();
}
