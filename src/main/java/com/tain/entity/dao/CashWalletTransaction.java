package com.tain.entity.dao;

import com.tain.util.TransactionType;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class CashWalletTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "cash_wallet_id", nullable = false)
    CashWallet cashWallet;

    @Column(nullable = false, unique = true)
    String transactionId;

    @Column(nullable = false)
    BigDecimal amount;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    TransactionType transactionType;

    String note;

    @Column(nullable = false)
    LocalDateTime createdAt = LocalDateTime.now();
}
