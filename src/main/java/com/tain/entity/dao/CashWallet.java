package com.tain.entity.dao;

import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity(name = "cash_wallet")
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class CashWallet {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", nullable = false)
    User user;

    @Column(nullable = false)
    BigDecimal balance;

    @Column(nullable = false)
    LocalDateTime lastBalanceUpdate = LocalDateTime.now();

}
