package com.tain.config;

import com.tain.service.security.UserSecurityService;
import com.tain.service.security.UserTokenProvider;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class JwtUserAuthenticationFilter extends HttpFilter {

    static String HEADER_STRING = "Authorization";

    static String TOKEN_PREFIX = "Bearer ";

    UserTokenProvider userTokenProvider;

    UserSecurityService userDetailsService;

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        try {
            String header = req.getHeader(HEADER_STRING);
            if (header != null && header.startsWith(TOKEN_PREFIX)
                    && SecurityContextHolder.getContext().getAuthentication() == null) {
                String authToken = header.replace(TOKEN_PREFIX, "");
                String username = userTokenProvider.getEmailFromToken(authToken);

                UserDetails userDetails = userDetailsService.loadUserByUsername(username);

                if (userTokenProvider.validateToken(authToken, userDetails)) {
                    UsernamePasswordAuthenticationToken authentication = userTokenProvider
                            .getAuthenticationToken(authToken, userDetails);
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
                    log.debug("Authenticated user {}, setting security context", username);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        } catch (RuntimeException e) {
            log.warn("Failed user authentication", e);
        }

        chain.doFilter(req, res);
    }

}
