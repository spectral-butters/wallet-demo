package com.tain.service;

import com.tain.entity.dao.User;
import com.tain.entity.dto.request.CreateUserRequest;
import com.tain.entity.dto.response.LoginResponse;
import com.tain.entity.dto.response.UserBalanceResponse;
import com.tain.exceptions.EntityAlreadyExistsException;
import com.tain.exceptions.EntityNotFoundException;
import com.tain.exceptions.InvalidRequestException;
import com.tain.repository.UserRepository;
import com.tain.service.security.AuthService;
import com.tain.util.ErrorCode;
import com.tain.util.UserRoles;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UserService {

    UserRepository userRepository;
    CashWalletService cashWalletService;
    BonusWalletService bonusWalletService;
    PasswordEncoder passwordEncoder;
    UserRoleService userRoleService;

    @Transactional
    public User create(CreateUserRequest request) {
        validateNewUser(request);
        User user = new User();
        user.setEmail(request.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setExternalId(findExternalId());
        user.setCashWallet(cashWalletService.buildWallet(user));
        user.setBonusWallet(bonusWalletService.buildWallet(user));
        user.getRoles().add(userRoleService.getByRole(UserRoles.USER));
        return save(user);
    }

    /**
     * Get player cash and bonus balance
     * @return TransactionResponse
     */
    public UserBalanceResponse getBalance(User user) {
        UserBalanceResponse response = new UserBalanceResponse();
        response.setCashBalance(cashWalletService.getBalance(user.getCashWallet()));
        response.setBonusBalance(bonusWalletService.getBalance(user.getBonusWallet()));
        response.setTotalBalance(response.getCashBalance().add(response.getBonusBalance()));
        return response;
    }

    private String findExternalId() {
        String extId = UUID.randomUUID().toString()
                .replaceAll("-", "")
                .substring(0, 8);
        try {
            getByExternalId(extId);
            return findExternalId();
        } catch (EntityNotFoundException e) {
            return extId;
        }
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User getByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User not found by email " + email, ErrorCode.USER_NOT_FOUND));
    }

    public User getByExternalId(String id) {
        return userRepository.findByExternalId(id)
                .orElseThrow(() -> new EntityNotFoundException("User not found by external id " + id, ErrorCode.USER_NOT_FOUND));
    }

    /**
     * Validates the request for creating a new user
     * @param request the request to validate
     */
    public void validateNewUser(CreateUserRequest request) {
        // verify that the email is not already in use
        userRepository.findByEmail(request.getEmail())
                .ifPresent(user -> {
                    throw new EntityAlreadyExistsException("User already exists", ErrorCode.ENTITY_ALREADY_EXISTS);
                });

        // verify that the passwords match
        if (!request.getPassword().equals(request.getConfirmPassword())) {
            throw new InvalidRequestException("password must be equal to confirm_password", ErrorCode.PASSWORD_MISMATCH);
        }
    }

    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User not found by id " + id, ErrorCode.USER_NOT_FOUND));
    }
}
