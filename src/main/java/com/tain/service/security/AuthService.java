package com.tain.service.security;

import com.tain.entity.dao.User;
import com.tain.entity.dto.request.LoginRequest;
import com.tain.entity.dto.response.LoginResponse;
import com.tain.service.UserService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthService {

	AuthenticationManager authenticationManager;

	UserTokenProvider userTokenProvider;

	UserService userService;

	public LoginResponse authenticate(LoginRequest request) {
		User user = userService.getByEmail(request.getEmail());
		Authentication userAuth = new UsernamePasswordAuthenticationToken(user.getEmail(), request.getPassword());
		Authentication authentication = authenticationManager.authenticate(userAuth);

		final String token = userTokenProvider.generateToken(authentication);

		LoginResponse response = new LoginResponse();
		response.setToken(token);

		return response;
	}

	public LoginResponse authenticate(String email, String password) {
		LoginRequest request = new LoginRequest();
		request.setEmail(email);
		request.setPassword(password);

		return authenticate(request);
	}

}
