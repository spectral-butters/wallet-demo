package com.tain.service;

import com.tain.entity.dao.User;
import com.tain.entity.dto.request.CreateUserRequest;
import com.tain.entity.dto.request.TransactionRequest;
import com.tain.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
@FieldDefaults(makeFinal = true, level = lombok.AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class PrefillService {

    UserService userService;
    TransactionService transactionService;
    TransactionRepository transactionRepository;

    @PostConstruct
    public void prefill() {
        if (!transactionRepository.findAll().isEmpty()) {
            return; // Skip prefill if the table is not empty
        }

        List<User> users = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            CreateUserRequest createUserRequest = getCreateUserRequest();
            createUserRequest.setEmail(createUserRequest.getEmail() + i);
            User user = userService.create(createUserRequest);
            users.add(user);
        }

        Random random = new Random();
        for (User user : users) {
            for (int i = 0; i < 10; i++) {
                TransactionRequest depositRequest = getTransactionRequest();
                depositRequest.setTransactionId(randomTransactionId());
                depositRequest.setAmount(new BigDecimal(random.nextInt(401) + 100)); // Range: 100 to 500
                depositRequest.setPlayerId(user.getExternalId());
                transactionService.processDeposit(depositRequest);
            }

            for (int i = 0; i < 5; i++) {
                TransactionRequest betRequest = getTransactionRequest();
                betRequest.setTransactionId(randomTransactionId());
                betRequest.setAmount(new BigDecimal(random.nextInt(51) + 50)); // Range: 50 to 100
                betRequest.setPlayerId(user.getExternalId());
                transactionService.processBet(betRequest);

                TransactionRequest winRequest = getTransactionRequest();
                winRequest.setTransactionId(betRequest.getTransactionId());
                winRequest.setAmount(new BigDecimal(random.nextInt(51) + 50)); // Range: 50 to 100
                winRequest.setPlayerId(user.getExternalId());
                transactionService.processWin(winRequest);
            }

            for (int i = 0; i < 2; i++) {
                TransactionRequest withdrawalRequest = getTransactionRequest();
                withdrawalRequest.setTransactionId(randomTransactionId());
                withdrawalRequest.setAmount(new BigDecimal(random.nextInt(101) + 100)); // Range: 100 to 200
                withdrawalRequest.setPlayerId(user.getExternalId());
                transactionService.processWithdrawal(withdrawalRequest);
            }
        }
    }

    private String randomTransactionId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }


    private CreateUserRequest getCreateUserRequest() {
        CreateUserRequest request = new CreateUserRequest();
        request.setEmail("example@example.com");
        request.setPassword("Password123!");
        request.setConfirmPassword("Password123!");
        return request;
    }

    private TransactionRequest getTransactionRequest() {
        TransactionRequest request = new TransactionRequest();
        request.setAmount(new BigDecimal("100"));
        request.setTransactionId(UUID.randomUUID().toString()
                .replaceAll("-", ""));
        request.setPlayerId("1");
        return request;
    }
}
