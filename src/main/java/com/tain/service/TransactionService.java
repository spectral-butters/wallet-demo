package com.tain.service;

import com.tain.entity.dao.GameRound;
import com.tain.entity.dao.Transaction;
import com.tain.entity.dao.User;
import com.tain.entity.dto.WalletTransactionDto;
import com.tain.entity.dto.request.TransactionRequest;
import com.tain.entity.dto.request.TransactionSearchRequest;
import com.tain.entity.dto.response.UserBalanceResponse;
import com.tain.entity.mapper.TransactionMapper;
import com.tain.exceptions.EntityNotFoundException;
import com.tain.exceptions.InvalidRequestException;
import com.tain.repository.TransactionRepository;
import com.tain.repository.specification.TransactionSpecification;
import com.tain.util.*;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class TransactionService {

    UserService userService;
    CashWalletService cashWalletService;
    BonusWalletService bonusWalletService;
    ApplicationProperties applicationProperties;
    TransactionRepository transactionRepository;
    GameRoundService gameRoundService;
    TransactionMapper transactionMapper = TransactionMapper.INSTANCE;

    public Transaction createTransaction(User user, WalletTransactionDto transactionDto, WalletType walletType) {
        Transaction transaction = transactionMapper.toTransaction(transactionDto);
        transaction.setUser(user);
        transaction.setWalletType(walletType);
        return transactionRepository.save(transaction);
    }

    /**
     * Main function to process deposit transaction
     * @param request TransactionRequest
     */
    @Transactional
    public UserBalanceResponse processDeposit(TransactionRequest request) {
        // TODO: check that deposit with this transaction id doesnt exist yet
        User user = userService.getByExternalId(request.getPlayerId());
        WalletTransactionDto transactionDto = transactionMapper.toWalletTransactionDto(request);
        transactionDto.setTransactionType(TransactionType.DEPOSIT);
        transactionDto.setNote("Player Deposit");
        cashWalletService.saveTransaction(user, transactionDto);
        createTransaction(user, transactionDto, WalletType.CASH);

        if (request.getAmount().compareTo(applicationProperties.getDepositBonusThreshold()) >= 0) {
            transactionDto.setAmount(request.getAmount().multiply(applicationProperties.getDepositBonusMultiplier()));
            transactionDto.setNote("Deposit Bonus");
            bonusWalletService.saveTransaction(user, transactionDto);
            createTransaction(user, transactionDto, WalletType.BONUS);
        }

        return userService.getBalance(user);
    }

    /**
     * Main function to process withdrawal transaction
     * @param request TransactionRequest
     */
    @Transactional
    public UserBalanceResponse processWithdrawal(TransactionRequest request) {
        User user = userService.getByExternalId(request.getPlayerId());

        BigDecimal balance = cashWalletService.getBalance(user.getCashWallet());

        if (balance.compareTo(request.getAmount()) < 0) {
            throw new InvalidRequestException("Insufficient funds", ErrorCode.INSUFFICIENT_FUNDS);
        }

        WalletTransactionDto transactionDto = transactionMapper.toWalletTransactionDto(request);
        transactionDto.setTransactionType(TransactionType.WITHDRAWAL);
        transactionDto.setNote("Player Withdrawal");
        transactionDto.setAmount(request.getAmount().negate());
        cashWalletService.saveTransaction(user, transactionDto);
        transactionDto.setAmount(request.getAmount());
        createTransaction(user, transactionDto, WalletType.CASH);

        return userService.getBalance(user);
    }

    @Transactional
    public UserBalanceResponse processBet(TransactionRequest request) {
        User user = userService.getByExternalId(request.getPlayerId());

        try {
            GameRound gameRound = gameRoundService.getByTransactionIdAndRoundType(request.getTransactionId(), RoundEventType.BET);
            if (gameRound.getAmount().compareTo(request.getAmount()) != 0)
                throw new InvalidRequestException(String.format("GameRound with transactionId %s and roundType %s already exists", request.getTransactionId(), RoundEventType.BET),
                    ErrorCode.GAME_ROUND_ALREADY_EXISTS);
            else {
                // TODO: return idempotent response, but for now
                throw new InvalidRequestException(String.format("GameRound with transactionId %s and roundType %s already exists", request.getTransactionId(), RoundEventType.BET),
                        ErrorCode.GAME_ROUND_ALREADY_EXISTS);
            }
        } catch (EntityNotFoundException e) {
            log.info("Processing bet for userId {} transactionId {} amount {}", user.getId(), request.getTransactionId(), request.getAmount());
        }

        UserBalanceResponse balanceResponse = userService.getBalance(user);

        if (balanceResponse.getTotalBalance().compareTo(request.getAmount()) < 0) {
            throw new InvalidRequestException("Insufficient funds", ErrorCode.INSUFFICIENT_FUNDS);
        }

        BigDecimal cashBalance = balanceResponse.getCashBalance();
        BigDecimal bonusBalance = balanceResponse.getBonusBalance();

        BigDecimal betAmount = request.getAmount();
        BigDecimal cashAmount = BigDecimal.ZERO;
        BigDecimal bonusAmount = BigDecimal.ZERO;

        // Calculate the proportions based on the specific bet amount
        if (cashBalance.compareTo(betAmount) >= 0) {
            cashAmount = betAmount;
        } else {
            cashAmount = cashBalance;
            bonusAmount = betAmount.subtract(cashBalance);
        }

        // Calculate the proportion as a percentage
        // Zero proportion means that the bet was made with bonus funds only
        BigDecimal proportion = BigDecimal.ZERO;
        if (betAmount.compareTo(BigDecimal.ZERO) > 0) {
            proportion = cashAmount.divide(betAmount, 4, RoundingMode.HALF_UP)
                    .multiply(BigDecimal.valueOf(100));
        }

        // Save the game round with the bet amount and proportion
        GameRound gameRound = new GameRound();
        gameRound.setUser(user);
        gameRound.setAmount(betAmount);
        gameRound.setProportion(proportion);
        gameRound.setRoundType(RoundEventType.BET);
        gameRound.setTransactionId(request.getTransactionId());
        gameRoundService.createGameRound(gameRound);

        // Update the cash wallet balance
        if (cashAmount.compareTo(BigDecimal.ZERO) > 0) {
            WalletTransactionDto cashTransactionDto = transactionMapper.toWalletTransactionDto(request);
            cashTransactionDto.setTransactionId("bet-" + request.getTransactionId());
            cashTransactionDto.setTransactionType(TransactionType.BET);
            cashTransactionDto.setNote("Player Bet");
            cashTransactionDto.setAmount(cashAmount.negate());
            cashWalletService.saveTransaction(user, cashTransactionDto);
            cashTransactionDto.setAmount(cashAmount);
            createTransaction(user, cashTransactionDto, WalletType.CASH);
        }

        // Update the bonus wallet balance
        if (bonusAmount.compareTo(BigDecimal.ZERO) > 0) {
            WalletTransactionDto bonusTransactionDto = transactionMapper.toWalletTransactionDto(request);
            bonusTransactionDto.setTransactionId("bet-" + request.getTransactionId());
            bonusTransactionDto.setTransactionType(TransactionType.BET);
            bonusTransactionDto.setNote("Player Bet");
            bonusTransactionDto.setAmount(bonusAmount.negate());
            bonusWalletService.saveTransaction(user, bonusTransactionDto);
            bonusTransactionDto.setAmount(bonusAmount);
            createTransaction(user, bonusTransactionDto, WalletType.BONUS);
        }

        return userService.getBalance(user);
    }

    @Transactional
    public UserBalanceResponse processWin(TransactionRequest request) {
        User user = userService.getByExternalId(request.getPlayerId());

        GameRound gameRound;

        try {
            gameRound = gameRoundService.getByTransactionIdAndRoundType(request.getTransactionId(), RoundEventType.BET);
        } catch (EntityNotFoundException e) {
            throw new InvalidRequestException(String.format("Bet with transactionId %s does not exist", request.getTransactionId()),
                    ErrorCode.GAME_ROUND_NOT_FOUND);
        }

        BigDecimal winAmount = request.getAmount();

        if (winAmount.compareTo(BigDecimal.ZERO) == 0) {
            GameRound lossGameRound = new GameRound();
            lossGameRound.setUser(user);
            lossGameRound.setAmount(BigDecimal.ZERO);
            lossGameRound.setProportion(BigDecimal.ZERO);
            lossGameRound.setRoundType(RoundEventType.WIN);
            lossGameRound.setTransactionId(request.getTransactionId());
            gameRoundService.createGameRound(lossGameRound);
            return userService.getBalance(user);
        }

        BigDecimal cashAmount = BigDecimal.ZERO;
        BigDecimal bonusAmount;

        // Calculate the cash and bonus amounts based on the saved proportion
        BigDecimal proportion = gameRound.getProportion();
        if (proportion.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal cashProportion = proportion.divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_UP);
            cashAmount = winAmount.multiply(cashProportion);
            bonusAmount = winAmount.subtract(cashAmount);
        } else {
            bonusAmount = winAmount; // If proportion is not applicable, allocate the entire amount to the bonus wallet
        }

        // Update the cash wallet balance
        if (cashAmount.compareTo(BigDecimal.ZERO) > 0) {
            WalletTransactionDto cashTransactionDto = transactionMapper.toWalletTransactionDto(request);
            cashTransactionDto.setTransactionId("win-" + request.getTransactionId());
            cashTransactionDto.setTransactionType(TransactionType.WIN);
            cashTransactionDto.setNote("Player Win");
            cashTransactionDto.setAmount(cashAmount);
            cashWalletService.saveTransaction(user, cashTransactionDto);
            createTransaction(user, cashTransactionDto, WalletType.CASH);
        }

        // Update the bonus wallet balance
        if (bonusAmount.compareTo(BigDecimal.ZERO) > 0) {
            WalletTransactionDto bonusTransactionDto = transactionMapper.toWalletTransactionDto(request);
            bonusTransactionDto.setTransactionId("win-" + request.getTransactionId());
            bonusTransactionDto.setTransactionType(TransactionType.WIN);
            bonusTransactionDto.setNote("Player Win");
            bonusTransactionDto.setAmount(bonusAmount);
            bonusWalletService.saveTransaction(user, bonusTransactionDto);
            createTransaction(user, bonusTransactionDto, WalletType.BONUS);
        }

        return userService.getBalance(user);
    }

    public Page<Transaction> search(TransactionSearchRequest request, Pageable pageable) {
        TransactionSpecification specification = new TransactionSpecification(request);
        return transactionRepository.findAll(specification, pageable);
    }

}
