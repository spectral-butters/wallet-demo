package com.tain.service;

import com.tain.entity.dao.CashWallet;
import com.tain.entity.dao.CashWalletTransaction;
import com.tain.entity.dao.User;
import com.tain.entity.dto.WalletTransactionDto;
import com.tain.entity.dto.request.TransactionRequest;
import com.tain.entity.mapper.TransactionMapper;
import com.tain.repository.CashWalletRepository;
import com.tain.repository.CashWalletTransactionRepository;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class CashWalletService {

    CashWalletRepository cashWalletRepository;
    CashWalletTransactionRepository cashWalletTransactionRepository;
    TransactionMapper transactionMapper = TransactionMapper.INSTANCE;
    public CashWalletTransaction saveTransaction(CashWalletTransaction transaction) {
        return cashWalletTransactionRepository.save(transaction);
    }

    public CashWalletTransaction saveTransaction(User user, WalletTransactionDto request) {
        CashWalletTransaction transaction = transactionMapper.toCashWalletTransaction(request);
        transaction.setCashWallet(user.getCashWallet());
        return saveTransaction(transaction);
    }

    @Transactional
    public CashWallet createWallet(User user) {
        CashWallet wallet = new CashWallet();
        wallet.setUser(user);
        wallet.setBalance(BigDecimal.ZERO);
        return cashWalletRepository.save(wallet);
    }

    public CashWallet buildWallet(User user) {
        CashWallet wallet = new CashWallet();
        wallet.setUser(user);
        wallet.setBalance(BigDecimal.ZERO);
        return wallet;
    }

    public BigDecimal getBalance(CashWallet cashWallet) {
        return cashWalletTransactionRepository.getBalance(cashWallet.getLastBalanceUpdate(), cashWallet.getId());
    }
}
