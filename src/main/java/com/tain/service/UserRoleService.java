package com.tain.service;

import com.tain.entity.dao.UserRole;
import com.tain.exceptions.EntityNotFoundException;
import com.tain.repository.UserRoleRepository;
import com.tain.util.ErrorCode;
import com.tain.util.UserRoles;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class UserRoleService {

    UserRoleRepository userRoleRepository;

    public UserRole getByRole(UserRoles role) {
        return userRoleRepository.findByRole(role)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Role %s not found", role),
                        ErrorCode.INTERNAL_ERROR));
    }

}
