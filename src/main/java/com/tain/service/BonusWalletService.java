package com.tain.service;

import com.tain.entity.dao.BonusWallet;
import com.tain.entity.dao.BonusWalletTransaction;
import com.tain.entity.dao.User;
import com.tain.entity.dto.WalletTransactionDto;
import com.tain.entity.mapper.TransactionMapper;
import com.tain.repository.BonusWalletRepository;
import com.tain.repository.BonusWalletTransactionRepository;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class BonusWalletService {

    BonusWalletRepository bonusWalletRepository;
    BonusWalletTransactionRepository bonusWalletTransactionRepository;

    TransactionMapper transactionMapper = TransactionMapper.INSTANCE;
    public BonusWalletTransaction saveTransaction(BonusWalletTransaction transaction) {
        return bonusWalletTransactionRepository.save(transaction);
    }

    public BonusWalletTransaction saveTransaction(User user, WalletTransactionDto walletTransactionDto) {
        BonusWalletTransaction transaction = transactionMapper.toBonusWalletTransaction(walletTransactionDto);
        transaction.setBonusWallet(user.getBonusWallet());
        return saveTransaction(transaction);
    }

    /**
     * Create new bonus wallet for user
     * @param user User
     * @return BonusWallet
     */
    @Transactional
    public BonusWallet createWallet(User user) {
        // Create bonus wallet
        BonusWallet wallet = new BonusWallet();
        wallet.setUser(user);
        wallet.setBalance(BigDecimal.ZERO);
        return bonusWalletRepository.save(wallet);
    }

    public BigDecimal getBalance(BonusWallet bonusWallet) {
        return bonusWalletTransactionRepository.getBalance(bonusWallet.getLastBalanceUpdate(), bonusWallet.getId());
    }

    public BonusWallet buildWallet(User user) {
        BonusWallet wallet = new BonusWallet();
        wallet.setUser(user);
        wallet.setBalance(BigDecimal.ZERO);
        return wallet;
    }
}
