package com.tain.service;

import com.tain.entity.dao.GameRound;
import com.tain.exceptions.EntityNotFoundException;
import com.tain.repository.GameRoundRepository;
import com.tain.util.ErrorCode;
import com.tain.util.RoundEventType;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class GameRoundService {

    GameRoundRepository gameRoundRepository;

    public GameRound createGameRound(GameRound gameRound) {
        return gameRoundRepository.save(gameRound);
    }

    public GameRound getByTransactionIdAndRoundType(String transactionId, RoundEventType roundEventType) {
        return gameRoundRepository.findByTransactionIdAndRoundType(transactionId, roundEventType)
                .orElseThrow(() -> new EntityNotFoundException(String
                        .format("GameRound with transactionId %s and roundType %s not found", transactionId, roundEventType)
                , ErrorCode.GAME_ROUND_NOT_FOUND));
    }

}
