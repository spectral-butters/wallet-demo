CREATE TABLE users
(
    id              BIGSERIAL PRIMARY KEY,
    email           VARCHAR(255) NOT NULL UNIQUE,
    password        VARCHAR(255) NOT NULL,
    external_id     VARCHAR(8)   NOT NULL UNIQUE,
    cash_wallet_id  BIGINT,
    bonus_wallet_id BIGINT,
    created_at      TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at      TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE user_roles
(
    id   SERIAL PRIMARY KEY,
    role VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE users_user_roles
(
    user_id      BIGINT  NOT NULL,
    user_role_id INTEGER NOT NULL,
    PRIMARY KEY (user_id, user_role_id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (user_role_id) REFERENCES user_roles (id)
);

CREATE TABLE bonus_wallet
(
    id                  BIGSERIAL PRIMARY KEY,
    user_id             BIGINT         NOT NULL,
    balance             NUMERIC(19, 2) NOT NULL,
    last_balance_update TIMESTAMP      NOT NULL,
    CONSTRAINT fk_bonus_wallet_user FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE cash_wallet
(
    id                  BIGSERIAL PRIMARY KEY,
    user_id             BIGINT         NOT NULL,
    balance             NUMERIC(19, 2) NOT NULL,
    last_balance_update TIMESTAMP      NOT NULL,
    CONSTRAINT fk_cash_wallet_user FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE bonus_wallet_transaction
(
    id               BIGSERIAL PRIMARY KEY,
    bonus_wallet_id  BIGINT         NOT NULL,
    transaction_id   VARCHAR(255)   NOT NULL UNIQUE,
    amount           NUMERIC(19, 2) NOT NULL,
    transaction_type VARCHAR(255)   NOT NULL,
    note             VARCHAR(255),
    created_at       TIMESTAMP      NOT NULL,
    CONSTRAINT fk_bonus_wallet_transaction FOREIGN KEY (bonus_wallet_id) REFERENCES bonus_wallet (id)
);

CREATE TABLE cash_wallet_transaction
(
    id               BIGSERIAL PRIMARY KEY,
    cash_wallet_id   BIGINT         NOT NULL,
    transaction_id   VARCHAR(255)   NOT NULL UNIQUE,
    amount           NUMERIC(19, 2) NOT NULL,
    transaction_type VARCHAR(255)   NOT NULL,
    note             VARCHAR(255),
    created_at       TIMESTAMP      NOT NULL,
    CONSTRAINT fk_cash_wallet_transaction FOREIGN KEY (cash_wallet_id) REFERENCES cash_wallet (id)
);

CREATE TABLE game_round
(
    id             BIGSERIAL PRIMARY KEY,
    user_id        BIGINT         NOT NULL,
    amount         DECIMAL(19, 2) NOT NULL,
    proportion     DECIMAL(19, 2) NOT NULL,
    round_type     VARCHAR(255)   NOT NULL,
    transaction_id VARCHAR(255)   NOT NULL,
    created_at     TIMESTAMP      NOT NULL,
    CONSTRAINT unique_transaction_id_round_type UNIQUE (transaction_id, round_type),
    CONSTRAINT unique_transaction_id_user_id_round_type UNIQUE (transaction_id, user_id, round_type),
    FOREIGN KEY (user_id) REFERENCES users (id)
);


CREATE TABLE transaction
(
    id               BIGSERIAL PRIMARY KEY,
    user_id          BIGINT         NOT NULL,
    amount           NUMERIC(19, 2) NOT NULL,
    transaction_id   VARCHAR(255)   NOT NULL,
    transaction_type VARCHAR(255)   NOT NULL,
    wallet_type      VARCHAR(255)   NOT NULL,
    note             VARCHAR(255),
    created_at       TIMESTAMP      NOT NULL,
    CONSTRAINT fk_transaction_user FOREIGN KEY (user_id) REFERENCES users (id)
);

ALTER TABLE users
    ADD CONSTRAINT fk_users_cash_wallet FOREIGN KEY (cash_wallet_id) REFERENCES cash_wallet (id),
    ADD CONSTRAINT fk_users_bonus_wallet FOREIGN KEY (bonus_wallet_id) REFERENCES bonus_wallet (id);