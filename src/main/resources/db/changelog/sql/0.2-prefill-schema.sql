INSERT INTO user_roles (role) VALUES ('USER');
INSERT INTO user_roles (role) VALUES ('ADMIN');

INSERT INTO users (email, password, external_id) VALUES ('admin@test.com', '$2a$10$jrpp0MYCfYf9H0R3/erUwOOQTfpGn1il0YoL00iAxKLVYPYiRRTO2', '12345678');
INSERT INTO users_user_roles (user_id, user_role_id) VALUES ((select id from users where email='admin@test.com'),
                                                              (select id from user_roles where role='ADMIN'));
