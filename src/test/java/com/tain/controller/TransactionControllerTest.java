package com.tain.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.jayway.jsonpath.TypeRef;
import com.tain.BaseControllerTest;
import com.tain.entity.dao.Transaction;
import com.tain.entity.dao.User;
import com.tain.entity.dto.TransactionResponseDto;
import com.tain.entity.dto.request.TransactionRequest;
import com.tain.entity.dto.request.TransactionSearchRequest;
import com.tain.entity.dto.response.UserBalanceResponse;
import com.tain.util.TestData;
import com.tain.util.TransactionType;
import com.tain.util.WalletType;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.*;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class TransactionControllerTest extends BaseControllerTest {

    @Test
    @SneakyThrows
    void testWithdraw() {
        TransactionRequest request = TestData.getTransactionRequest();
        User user = TestData.getUser();
        request.setPlayerId(user.getExternalId());
        when(userService.getById(any())).thenReturn(user);
        String requestJson = objectMapper.writeValueAsString(request);

        UserBalanceResponse expectedResponse = TestData.getUserBalanceResponse();

        when(transactionService.processWithdrawal(request))
                .thenReturn(expectedResponse);

        MvcResult result = mockMvc.perform(postWithUser("/transaction/withdraw")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk())
                .andReturn();

        UserBalanceResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), UserBalanceResponse.class);

        assertEquals(expectedResponse, response);

    }

    @Test
    @SneakyThrows
    void testBet() {
        TransactionRequest request = TestData.getTransactionRequest();
        User user = TestData.getUser();
        request.setPlayerId(user.getExternalId());
        when(userService.getById(any())).thenReturn(user);
        String requestJson = objectMapper.writeValueAsString(request);

        UserBalanceResponse expectedResponse = TestData.getUserBalanceResponse();

        when(transactionService.processBet(request))
                .thenReturn(expectedResponse);

        MvcResult result = mockMvc.perform(postWithUser("/transaction/bet")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk())
                .andReturn();

        UserBalanceResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), UserBalanceResponse.class);

        assertEquals(expectedResponse, response);

    }

    @Test
    @SneakyThrows
    void testDeposit() {
        TransactionRequest request = TestData.getTransactionRequest();
        User user = TestData.getUser();
        request.setPlayerId(user.getExternalId());
        when(userService.getById(any())).thenReturn(user);
        String requestJson = objectMapper.writeValueAsString(request);

        UserBalanceResponse expectedResponse = TestData.getUserBalanceResponse();

        when(transactionService.processDeposit(request))
                .thenReturn(expectedResponse);

        MvcResult result = mockMvc.perform(postWithUser("/transaction/deposit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk())
                .andReturn();

        UserBalanceResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), UserBalanceResponse.class);

        assertEquals(expectedResponse, response);

    }

    @Test
    @SneakyThrows
    void testDeposit_Forbidden() {
        TransactionRequest request = TestData.getTransactionRequest();

        String requestJson = objectMapper.writeValueAsString(request);

        UserBalanceResponse expectedResponse = TestData.getUserBalanceResponse();

        when(transactionService.processBet(request))
                .thenReturn(expectedResponse);

        // When no auth provided
        // 403 must be returned
        MvcResult result = mockMvc.perform(post("/transaction/deposit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isForbidden())
                .andReturn();

        verify(transactionService, times(0)).processDeposit(any());

    }

    @Test
    void testSearchTransactions() throws Exception {
        // Prepare test data
        TransactionType transactionType = TransactionType.DEPOSIT;
        String transactionId = "123456";
        WalletType walletType = WalletType.CASH;
        String note = "Test transaction";
        BigDecimal amountFrom = BigDecimal.valueOf(100);
        BigDecimal amountTo = BigDecimal.valueOf(500);
        LocalDateTime from = LocalDateTime.parse("2023-01-01T00:00:00");
        LocalDateTime to = LocalDateTime.parse("2023-02-01T00:00:00");

        // Prepare the expected search request
        TransactionSearchRequest expectedSearchRequest = TransactionSearchRequest.builder()
                .userId(TestData.getUserCustomUserDetails().getId())
                .transactionType(transactionType)
                .transactionId(transactionId)
                .walletType(walletType)
                .note(note)
                .amountFrom(amountFrom)
                .amountTo(amountTo)
                .from(from)
                .to(to)
                .build();

        // Prepare the expected list of transactions
        List<Transaction> expectedTransactions = Arrays.asList(
                new Transaction(),
                new Transaction()
        );
        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.ASC, "id");
        Page<Transaction> expectedPage = new PageImpl<>(expectedTransactions, pageable, expectedTransactions.size());

        // Mock the service method
        when(transactionService.search(expectedSearchRequest, pageable)).thenReturn(expectedPage);

        // Perform the GET request
        MvcResult result = mockMvc.perform(getWithUser("/transaction/search")
                        .param("transaction_type", transactionType.name())
                        .param("transaction_id", transactionId)
                        .param("wallet_type", walletType.name())
                        .param("note", note)
                        .param("amount_from", amountFrom.toString())
                        .param("amount_to", amountTo.toString())
                        .param("created_from", "2023-01-01T00:00:00")
                        .param("created_to", "2023-02-01T00:00:00")
                        .param("page", "0")
                        .param("size", "10")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        log.info("Response: {}", result.getResponse().getContentAsString());

        String json = result.getResponse().getContentAsString();
        JsonNode node = objectMapper.readTree(json);
        JsonNode userNode = node.at("/content");
        String userJson = userNode.toString();
        List<TransactionResponseDto> response = objectMapper.readValue(userJson, new TypeReference<>() {
        });

        assertEquals(2, response.size());

        // Verify that the service method was called with the expected search request
        verify(transactionService).search(expectedSearchRequest, pageable);
    }

}

