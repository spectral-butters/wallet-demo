package com.tain.integration;

import com.tain.BaseIntegrationTest;
import com.tain.entity.dao.BonusWallet;
import com.tain.entity.dao.CashWallet;
import com.tain.entity.dao.User;
import com.tain.entity.dao.UserRole;
import com.tain.entity.dto.WalletTransactionDto;
import com.tain.entity.dto.request.CreateUserRequest;
import com.tain.entity.dto.request.TransactionRequest;
import com.tain.entity.dto.response.UserBalanceResponse;
import com.tain.entity.mapper.TransactionMapper;
import com.tain.util.TestData;
import com.tain.util.TransactionType;
import com.tain.util.UserRoles;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class UserServiceIntegrationTest extends BaseIntegrationTest {

    @Test
    void testCreate() {
        CreateUserRequest request = TestData.getCreateUserRequest();
        User user = userService.create(request);
        assertNotNull(user.getId());
        assertNotNull(user.getCashWallet().getId());
        assertNotNull(user.getBonusWallet().getId());
        assertNotNull(user.getExternalId());

        // Verify password encoded
        assertTrue(passwordEncoder.matches(request.getPassword(), user.getPassword()));
        assertEquals(UserRoles.USER, user.getRoles().get(0).getRole());
    }

    @Test
    void testGetBalance() {
        User user = userService.create(TestData.getCreateUserRequest());
        TransactionRequest request = TestData.getTransactionRequest();
        request.setPlayerId(user.getExternalId());

        WalletTransactionDto transactionDto = TransactionMapper.INSTANCE.toWalletTransactionDto(request);
        transactionDto.setTransactionType(TransactionType.DEPOSIT);

        cashWalletService.saveTransaction(user, transactionDto);

        bonusWalletService.saveTransaction(user, transactionDto);

        final UserBalanceResponse result = userService.getBalance(user);

        assertEquals(0, request.getAmount().compareTo(result.getCashBalance()));
        assertEquals(0, request.getAmount().compareTo(result.getBonusBalance()));
        assertEquals(0, request.getAmount().add(request.getAmount()).compareTo(result.getTotalBalance()));
    }

}
