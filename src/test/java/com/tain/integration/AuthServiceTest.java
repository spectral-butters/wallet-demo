package com.tain.integration;

import com.tain.BaseIntegrationTest;
import com.tain.entity.dao.User;
import com.tain.entity.dto.request.CreateUserRequest;
import com.tain.entity.dto.response.LoginResponse;
import com.tain.util.TestData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
public class AuthServiceTest extends BaseIntegrationTest {

    @Test
    void testLogin() {
        CreateUserRequest request = TestData.getCreateUserRequest();
        User user = userService.create(request);

        LoginResponse response = authService.authenticate(request.getEmail(), request.getPassword());
        assertNotNull(response.getToken());
        assertEquals(userTokenProvider.getEmailFromToken(response.getToken()), user.getEmail());
    }

}
