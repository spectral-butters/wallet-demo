package com.tain.integration;

import com.tain.BaseIntegrationTest;
import com.tain.entity.dao.CashWallet;
import com.tain.entity.dao.User;
import com.tain.entity.dto.WalletTransactionDto;
import com.tain.entity.dto.request.TransactionRequest;
import com.tain.entity.dto.response.UserBalanceResponse;
import com.tain.entity.mapper.TransactionMapper;
import com.tain.exceptions.ErrorCodeException;
import com.tain.exceptions.InvalidRequestException;
import com.tain.util.ErrorCode;
import com.tain.util.TestData;
import com.tain.util.TransactionType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
public class TransactionServiceIntegrationTest extends BaseIntegrationTest {

    @Test
    void testProcessDeposit() {
        User user = userService.create(TestData.getCreateUserRequest());
        TransactionRequest request = TestData.getTransactionRequest();
        request.setPlayerId(user.getExternalId());
        // Set minimum amount to test deposit bonus
        request.setAmount(new BigDecimal("100"));

        WalletTransactionDto transactionDto = TransactionMapper.INSTANCE.toWalletTransactionDto(request);
        transactionDto.setTransactionType(TransactionType.DEPOSIT);

        UserBalanceResponse initialBalance = userService.getBalance(user);

        // Process the deposit
        UserBalanceResponse result = transactionService.processDeposit(request);

        // Assert the updated balances
        assertEquals(0, initialBalance.getCashBalance().add(request.getAmount()).compareTo(result.getCashBalance()));
        assertEquals(0, initialBalance.getBonusBalance().add(request.getAmount()).compareTo(result.getBonusBalance())); // No bonus transaction in this case

        BigDecimal newAmount = new BigDecimal("99");
        request.setAmount(newAmount);
        request.setTransactionId(request.getTransactionId() + "_new");
        UserBalanceResponse newResult = transactionService.processDeposit(request);

        assertEquals(0, result.getCashBalance().add(newAmount).compareTo(newResult.getCashBalance()));
        // Bonus should not be applied
        assertEquals(0, result.getBonusBalance().compareTo(newResult.getBonusBalance()));
    }

    @Test
    void testProcessWithdrawal() {
        // Create a user and their cash wallet
        User user = userService.create(TestData.getCreateUserRequest());
        CashWallet cashWallet = user.getCashWallet();

        // Top up wallet
        TransactionRequest depositRequest = TestData.getTransactionRequest();
        depositRequest.setAmount(new BigDecimal("1000.00"));
        WalletTransactionDto transactionDto = TransactionMapper.INSTANCE.toWalletTransactionDto(depositRequest);
        transactionDto.setTransactionType(TransactionType.DEPOSIT);
        cashWalletService.saveTransaction(user, transactionDto);

        // Define the withdrawal amount
        BigDecimal withdrawalAmount = new BigDecimal("600.00");

        // Create a transaction request for withdrawal
        TransactionRequest withdrawalRequest = TestData.getTransactionRequest();
        withdrawalRequest.setTransactionId(withdrawalRequest.getTransactionId() + "_new");
        withdrawalRequest.setPlayerId(user.getExternalId());
        withdrawalRequest.setAmount(withdrawalAmount);

        // Perform the withdrawal
        UserBalanceResponse result = transactionService.processWithdrawal(withdrawalRequest);

        // Assert the updated balance
        BigDecimal expectedBalance = depositRequest.getAmount().subtract(withdrawalAmount);
        assertEquals(0, expectedBalance.compareTo(result.getCashBalance()));

        // Verify that insufficient funds throws an exception
        ErrorCodeException errorCodeException = assertThrows(InvalidRequestException.class, () -> {
            withdrawalRequest.setTransactionId(withdrawalRequest.getTransactionId() + "_new");
            transactionService.processWithdrawal(withdrawalRequest);
        });

        assertEquals(ErrorCode.INSUFFICIENT_FUNDS, errorCodeException.getErrorCode());
    }

    @Test
    void testCasinoFlow() {
        // Create user
        User user = userService.create(TestData.getCreateUserRequest());
        // Make a deposit to top up the cash wallet and bonus wallet
        // Bonus is applied
        BigDecimal topUpAmount = new BigDecimal("1000.00");
        TransactionRequest depositRequest = TestData.getTransactionRequest();
        depositRequest.setAmount(topUpAmount);
        depositRequest.setPlayerId(user.getExternalId());
        transactionService.processDeposit(depositRequest);

        // Create a transaction request for bet
        BigDecimal betAmount = new BigDecimal("100.00");
        TransactionRequest betRequest = TestData.getTransactionRequest();
        betRequest.setTransactionId(betRequest.getTransactionId() + "_new");
        betRequest.setPlayerId(user.getExternalId());

        // Perform the bet
        // Balance Must be only subtracted from cash wallet
        UserBalanceResponse betResponse = transactionService.processBet(betRequest);
        assertEquals(0, topUpAmount.subtract(betAmount).compareTo(betResponse.getCashBalance()));
        assertEquals(0, topUpAmount.compareTo(betResponse.getBonusBalance()));

        // Create a transaction request for win
        // Use the same transaction ID as the bet
        BigDecimal winAmount = new BigDecimal("50.00");
        TransactionRequest winRequest = TestData.getTransactionRequest();
        winRequest.setAmount(winAmount);
        winRequest.setPlayerId(user.getExternalId());
        winRequest.setTransactionId(betRequest.getTransactionId());

        // Perform the win
        UserBalanceResponse winResponse = transactionService.processWin(winRequest);

        // Balance must only be added to cash wallet
        assertEquals(0, betResponse.getCashBalance().add(winAmount).compareTo(winResponse.getCashBalance()));
        assertEquals(0, betResponse.getBonusBalance().compareTo(winResponse.getBonusBalance()));

        BigDecimal currentCash = winResponse.getCashBalance();
        BigDecimal currentBonus = winResponse.getBonusBalance();

        // When bet amount is higher than cash balance, bonus balance must be used to cover full amount
        // 1000 - 100 + 50 = 950 (current cash balance) so 50 must be used from bonus balance
        betAmount = new BigDecimal("1000.00");

        betRequest.setTransactionId(betRequest.getTransactionId() + "_new");
        betRequest.setAmount(betAmount);
        UserBalanceResponse betResponse2 = transactionService.processBet(betRequest);

        assertEquals(0, BigDecimal.ZERO.compareTo(betResponse2.getCashBalance()));
        assertEquals(0, currentBonus.subtract(betAmount.subtract(currentCash)).compareTo(betResponse2.getBonusBalance()));

        // 950 from cash and 50 from bonus so proportion = 0.95
        winAmount = new BigDecimal("100.00");
        // so 95 goes to cash and 5 to bonus
        winRequest.setTransactionId(betRequest.getTransactionId());
        winRequest.setAmount(winAmount);
        UserBalanceResponse winResponse2 = transactionService.processWin(winRequest);

        BigDecimal updatedCashAmount = betResponse2.getCashBalance().add(winAmount.multiply(new BigDecimal("0.95")));
        BigDecimal updatedBonusAmount = betResponse2.getBonusBalance().add(winAmount.multiply(new BigDecimal("0.05")));

        assertEquals(0, winResponse2.getCashBalance().compareTo(updatedCashAmount));
        assertEquals(0, winResponse2.getBonusBalance().compareTo(updatedBonusAmount));

        // When bet amount is higher than cash balance and bonus balance, an exception must be thrown
        betAmount = new BigDecimal("10000.00");
        betRequest.setTransactionId(betRequest.getTransactionId() + "_new");
        betRequest.setAmount(betAmount);
        assertThrows(InvalidRequestException.class, () -> {
            transactionService.processBet(betRequest);
        });
    }

}
