package com.tain;

import com.tain.repository.*;
import com.tain.service.*;
import com.tain.service.security.AuthService;
import com.tain.service.security.UserTokenProvider;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.transaction.Transactional;

@Transactional
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class BaseIntegrationTest {

    @SpyBean
    UserService userService;

    @SpyBean
    AuthService authService;

    @Autowired
    UserTokenProvider userTokenProvider;

    @SpyBean
    TransactionService transactionService;

    @SpyBean
    GameRoundService gameRoundService;

    @SpyBean
    CashWalletService cashWalletService;

    @SpyBean
    BonusWalletService bonusWalletService;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    GameRoundRepository gameRoundRepository;

    @Autowired
    CashWalletRepository cashWalletRepository;

    @Autowired
    BonusWalletRepository bonusWalletRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    static PostgreSQLContainer postgreSQLContainer;

    @BeforeAll
    public static void setup() {
        if (postgreSQLContainer == null) {
            postgreSQLContainer = new PostgreSQLContainer("postgres:11");
            postgreSQLContainer.start();
        }
        System.setProperty("spring.datasource.url", postgreSQLContainer.getJdbcUrl());
        System.setProperty("spring.datasource.username", postgreSQLContainer.getUsername());
        System.setProperty("spring.datasource.password", postgreSQLContainer.getPassword());
        // solution for postgres to many clients exception
        System.setProperty("spring.datasource.hikari.maximum-pool-size", "2");
    }
}
