package com.tain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tain.service.*;
import com.tain.service.security.AuthService;
import com.tain.service.security.CustomUserDetails;
import com.tain.service.security.UserSecurityService;
import com.tain.service.security.UserTokenProvider;
import com.tain.util.TestData;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

@WebMvcTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class BaseControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    AuthService authService;

    @MockBean
    PasswordEncoder passwordEncoder;

    @MockBean
    UserSecurityService userSecurityService;

    @MockBean
    UserTokenProvider userTokenProvider;

    @MockBean
    BonusWalletService bonusWalletService;

    @MockBean
    CashWalletService cashWalletService;

    @MockBean
    GameRoundService gameRoundService;

    @MockBean
    TransactionService transactionService;

    @MockBean
    UserRoleService userRoleService;

    @MockBean
    UserService userService;

    protected MockHttpServletRequestBuilder getWith(String url, CustomUserDetails details) {
        return MockMvcRequestBuilders.get(url).with(user(details));
    }

    protected MockHttpServletRequestBuilder postWith(String url, CustomUserDetails details) {
        return MockMvcRequestBuilders.post(url).with(user(details));
    }

    protected MockHttpServletRequestBuilder getWithUser(String url) {
        return MockMvcRequestBuilders.get(url).with(user(TestData.getUserCustomUserDetails()));
    }

    protected MockHttpServletRequestBuilder getWithAdmin(String url) {
        return MockMvcRequestBuilders.get(url).with(user(TestData.getAdminCustomUserDetails()));
    }

    protected MockHttpServletRequestBuilder postWithAdmin(String url) {
        return MockMvcRequestBuilders.post(url).with(user(TestData.getAdminCustomUserDetails()));
    }

    protected MockHttpServletRequestBuilder postWithUser(String url) {
        return MockMvcRequestBuilders.post(url).with(user(TestData.getUserCustomUserDetails()));
    }


}
