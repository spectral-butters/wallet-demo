package com.tain.util;

import com.tain.entity.dao.User;
import com.tain.entity.dto.request.CreateUserRequest;
import com.tain.entity.dto.request.TransactionRequest;
import com.tain.entity.dto.response.UserBalanceResponse;
import com.tain.service.security.CustomUserDetails;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.UUID;

public class TestData {

    public static User getUser() {
        User user = new User();
        user.setEmail("example@example.com");
        user.setPassword("password123");
        user.setExternalId("externalId");
        return user;
    }

    public static CreateUserRequest getCreateUserRequest() {
        CreateUserRequest request = new CreateUserRequest();
        request.setEmail("example@example.com");
        request.setPassword("Password123");
        request.setConfirmPassword("Password123");
        return request;
    }

    public static TransactionRequest getTransactionRequest() {
        TransactionRequest request = new TransactionRequest();
        request.setAmount(new BigDecimal("100"));
        request.setTransactionId(UUID.randomUUID().toString()
                .replaceAll("-", ""));
        request.setPlayerId("1");
        return request;
    }

    public static CustomUserDetails getUserCustomUserDetails() {
        return new CustomUserDetails(1L, "user", "Password",
                Collections.singletonList(new SimpleGrantedAuthority("USER")));
    }

    public static CustomUserDetails getAdminCustomUserDetails() {
        return new CustomUserDetails(1L, "user", "Password",
                Collections.singletonList(new SimpleGrantedAuthority("ADMIN")));
    }

    public static UserBalanceResponse getUserBalanceResponse() {
        UserBalanceResponse response = new UserBalanceResponse();
        response.setCashBalance(new BigDecimal("100"));
        response.setBonusBalance(new BigDecimal("50"));
        response.setCashBalance(new BigDecimal("150"));
        return response;
    }
}
