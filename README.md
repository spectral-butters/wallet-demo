## App installation with docker:
1. Clone the repository
2. Run `docker-compose up --build`
This will create local PostgreSQL database and build the app image.
3. App will be accessible via http://localhost:9000  
In the repository you can find **Postman collection** with all the requests.
[Wallet local demo.postman_collection.json](Wallet%20local%20demo.postman_collection.json)  
Simply Import it in postman and run the requests.  
**Note**: On User creation, User Login, Admin Login - token is automatically fetched and applied to all necessary requests, so you don't need to manually insert Bearer token into requests.
4. To stop all containers run `docker rm $(docker stop $(docker ps -q))`

## App installation with maven:
1. Clone the repository
2. Create local PostgreSQL database according to [application.yml](src/main/resources/application.yml) file
3. Run `mvn clean install`
4. Run `mvn spring-boot:run`

## Application endpoints:
All the endpoints are described in [Wallet local demo.postman_collection.json](Wallet%20local%20demo.postman_collection.json) file.  

**Note**: On User creation, User Login, Admin Login - token is automatically fetched and applied to all necessary requests, so you don't need to manually insert Bearer token into requests.

## Application is prefilled
With 50 users with   
email = `example@example.com` + number from 1 to 50 (e.g. example@example.com12)  
password = `Password123!`

And admin user with credentials  
`{
"email":"admin@test.com",
"password":"mYpAssw0rd123!"
}`

### Api list:
`POST /user` - create user  
`GET /user/balance` - get user balance of logged in user  
`POST /auth/login` - login user  
`POST /transaction/bet` - make bet  
`POST /transaction/deposit` - make deposit  
`POST /transaction/withdraw` - make withdraw  
`GET /transaction/search` - get transaction history of logged in user  
`POST /admin/transaction/win` - record win (admin only)  
`POST /admin/transaction/search` - get transaction history of any user (admin only)  


## Application assumptions:
1. One round has only one bet and one win
2. Application works with one currency
3. If only player should be allowed to create bet / deposit / withdrawal then playerId from the request is ignored and taken from auth token. However it's left with @NotEmpty to speed up test task
4. Bonuses are configurable via application.yml file.   
`application:`  
   `depositBonusThreshold: 100` - controls deposit bonus threshold  
   `depositBonusMultiplier: 1` - controls deposit bonus multiplier

## Application improvements:
1. Add more tests
2. Add more validations (e.g. check if transaction doesn't exist before processing it)
3. Add wallet balance update to cut getBalance execution time, either with cron job or based on a trigger.
4. Split TransactionService into Transaction and Wallet services process transactions and GameRounds separately
5. Move signKey to external secret storage (e.g. AWS secrets manager)
6. Add idempotency
7. Add more logging
8. Add Swagger
   