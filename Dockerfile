# Use a multi-stage build for efficient Docker image size
# Build stage
FROM maven:3.8.1-openjdk-16 as build
WORKDIR /app
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src ./src
RUN mvn clean install -DskipTests=true

# Run stage
FROM openjdk:16
WORKDIR /app
EXPOSE 9000
COPY --from=build /app/target/wallet.jar ./wallet.jar
ENTRYPOINT ["java", "-jar", "wallet.jar"]
